# Data Forest Module Code

The following code is the arduino code for the module of the project "IoTree", from the 2019 NASA Space Apps Challenge

## Installation

Using the [Arduino IDE](https://www.arduino.cc/en/Main/Software), open the .ino archive, connect your arduino board and press ```upload``` on the IDE to start the project.

## Components

- Protoboard
- DHT11
- HC06
- STM32
- Jumpers


## Contribuitors
- Data Forest Team
- ICON Lab
- Onda Digital Lab
- Jeferson Lima


## License
[MIT](https://choosealicense.com/licenses/mit/)