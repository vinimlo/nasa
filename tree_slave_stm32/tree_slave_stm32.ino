#include <DHT.h>

#define DHTPIN PA1
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);

long interval = 5000;
long previousTime = 0;

const int ledPin = PA0;
char receivedData = 0;

int i = 0;

void setup() {
  dht.begin();
  Serial1.begin(9600);
  Serial1.print("IOTREE\n");
  pinMode(ledPin, OUTPUT);
}

void loop() {
  unsigned long currentTime = millis();
  
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  
  if (currentTime - previousTime > interval){
    previousTime = currentTime;
    Serial1.print("U: ");
    Serial1.print(h);
    Serial1.print(" %");
    Serial1.print('\t');
    Serial1.print("T: ");
    Serial1.print(t);
    Serial1.print(" C");
    Serial1.println();
  }

  if (Serial1.available() > 0) {
    receivedData = Serial1.read();
    if (receivedData == '1') {
      digitalWrite(ledPin, HIGH);
    }

    else if (receivedData == '0') {
      digitalWrite(ledPin, LOW);
    }
  }
}
